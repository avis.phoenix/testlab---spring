# Read me - Test Lab Spring

This is a Test Lab of [Spring Boot 2.0](https://spring.io) based on Udemy course [Spring Framework 5](https://www.udemy.com/share/1003TeAksYcFhXQ3Q=/).

Goals of the project:
- Web Page created using technology for Java and HTML5
- Can create, edit and delete client
- Have a list of products
- Can create, delete a Invoice for each client
- Each client have a profile page and view his invoices
- In the create invoice page we can search each product with a autocomplete input
- Can download in Excel and PDF format each invoice
- Can download in CSV, XML and JSON format the list of clients
- Has two users, each witch diferent role, an administrator user, and a normal user
- Only the administrator user can create, delete, or edit each clients or invoice.
- The guest user (no login) only can view the list of clients
- Partial support of 3 languages
- Have an API REST to get a json and xml representation of clients
- Test technologies

Technologies that use:
- JPA to connect in MySQL via JDBC (or another database)
- Spring Security
- BCrypt
- Spring Boot
- Thymeleaf
- Bootstrap
- Locale
- View technologies
    - OpenPDF
    - Apache POI
    - Super CSV
    - Jaxb2Marshaller
    - Jackson JSON
- JWT (JWT Branch)
- Angular (comming soon)
- Webflux (comming soon)
- Redux (comming soon)

## Important Data

Users:

| User Name | Password |
| :-------: | :------: |
| admin     | 12345    |
| andres    | 12345    |


## Notes about JSON and XML

Both of this implementations need a special care with the cyclic items (in this example clients have invoices (factura) and invoices have clients). In order to avoid an infinity loop while the libraries try to generate this formats we need to use an annotations in the entities class (in this case, Cliente.java and Factura.java).

### XML

With XML we just need to add @XmlTransient annotation above _public Cliente getCliente()_ declaration to cut the loop.

### JSON

In this case we need to specify more annotations:
 - @JsonFormat(pattern="yyyy/MM/dd") above the date (_createAt_) in the client class to specify the output format in the json
 - @JsonManagedReference above _private List<Factura> facturas;_ in the client class in combination of @JsonBackReference above _private Cliente cliente;_ in the invoice class in order to avoid the loop. (We can use just the @JsonIgnore above _private List<Factura> facturas;_ alternatively, but in this case the JSON don't include the invoices of each client)
 - @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) above _private Producto producto;_ in the item of invoices class to avoid an error caused by the lazy label in the _private Producto producto;_ variable.
