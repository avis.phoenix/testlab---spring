﻿package test.avisphoenix.hellojpa.controllers;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.net.MalformedURLException;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import test.avisphoenix.hellojpa.models.entity.Cliente;
import test.avisphoenix.hellojpa.models.services.IClienteService;
import test.avisphoenix.hellojpa.models.services.IUploadFileService;
import test.avisphoenix.hellojpa.utils.paginator.PageRender;
import test.avisphoenix.hellojpa.view.xml.ClienteList;

@Controller
@SessionAttributes("cliente")
public class ClienteController {

	@Autowired
	private IClienteService clienteService;
	
	private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());
	
	@Autowired
	private IUploadFileService uploadService;
	
	@Autowired
	private MessageSource messageSource;

	@RequestMapping(value = "/listar", method = RequestMethod.GET)
	public String listar(@RequestParam(name="page", defaultValue="0") int page, Model model,
			Authentication authentication, HttpServletRequest request, Locale locale) {
		
		Pageable pageRequest = PageRequest.of(page, 4);
		
		Page<Cliente> clientes = clienteService.findAll(pageRequest);
		
		PageRender<Cliente> pageRender = new PageRender<>("/listar", clientes);
		
		model.addAttribute("titulo", messageSource.getMessage("text.cliente.listar.titulo", null, locale));
		model.addAttribute("clientes", clienteService.findAll(pageRequest));
		model.addAttribute("page", pageRender);
		if (authentication != null) {
			model.addAttribute("user", authentication.getName());
		} else  {
			model.addAttribute("user", null);
		}
		
		if ((authentication != null && SecurityContextHolder.getContext() != null && SecurityContextHolder.getContext().getAuthentication() != null ) && authentication.getName().compareTo(SecurityContextHolder.getContext().getAuthentication().getName()) != 0) {
			model.addAttribute("danger", "El usuario no coincide: " + authentication.getName() + "!=" + SecurityContextHolder.getContext().getAuthentication().getName());
		} else if (authentication != null && ( SecurityContextHolder.getContext() == null || SecurityContextHolder.getContext().getAuthentication() == null )  ) {
			model.addAttribute("danger", "El usuario no coincide: " + authentication.getName()  + "!= null ");
		} else if (authentication == null && ( SecurityContextHolder.getContext() != null && SecurityContextHolder.getContext().getAuthentication() == null )  ) {
			model.addAttribute("danger", "El usuario no coincide");
		}
		
		if (hasRole("ROLE_ADMIN")) {
			model.addAttribute("rol", "admin");
		}
		
		SecurityContextHolderAwareRequestWrapper context = new SecurityContextHolderAwareRequestWrapper(request, "ROLE_");
		if (context.isUserInRole("ADMIN")) {
			model.addAttribute("rol2", "admin");
		}
		
		if (request.isUserInRole("ROLE_ADMIN")){
			model.addAttribute("rol3", "admin");
		}
		
		return "listar";
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/form")
	public String crear(Map<String, Object> model, Locale locale) {

		Cliente cliente = new Cliente();

		model.put("titulo", messageSource.getMessage("text.cliente.form.titulo.crear", null, locale));
		model.put("cliente", cliente);

		return "form";
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/form", method = RequestMethod.POST)
	public String guardar(@Valid Cliente cliente, BindingResult result, Map<String, Object> model, @RequestParam("file") MultipartFile foto, RedirectAttributes flash, SessionStatus status, Locale locale) {

		String salida = "form";
		if (!result.hasErrors()) {
			String mensaje = cliente.getId() != null ? messageSource.getMessage("text.cliente.flash.editar.success", null, locale) :messageSource.getMessage("text.cliente.flash.crear.success", null, locale);
			if (!foto.isEmpty()) {
				if ( cliente.getId() != null && ( cliente.getFoto() != null &&  cliente.getFoto().length() > 0)) {
					if (!uploadService.delete(cliente.getFoto())) {
						logger.info("No se pudo borrar el archivo: " + cliente.getFoto());
					}
				}
				//Path directorioRecursos = Paths.get("src/main/resources/static/uploads");
				//String rootPath = directorioRecursos.toFile().getAbsolutePath();
				//String rootPath = "/tmp/java_tester/";
				//String rootPath = "D:/java_tester/";
				
				String uniqueFileName = "";
				try {
					uniqueFileName = uploadService.copy(foto);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				flash.addFlashAttribute("info", messageSource.getMessage("text.cliente.flash.foto.subir.success", null, locale));
				
				cliente.setFoto(uniqueFileName);
			}
			clienteService.save(cliente);
			status.setComplete();
			flash.addFlashAttribute("sucess", mensaje );
			salida = "redirect:/listar";
		}
		model.put("titulo", messageSource.getMessage("text.cliente.form.titulo.crear", null, locale));

		return salida;
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/form/{id}")
	public String editar(@PathVariable(value = "id") Long id, Map<String, Object> model, RedirectAttributes flash, Locale locale) {

		String salida = "redirect:/listar";

		if (id > 0) {
			Cliente cliente = clienteService.find(id);
			if (cliente != null) {
				model.put("titulo", messageSource.getMessage("text.cliente.form.titulo.editar", null, locale));
				model.put("cliente", cliente);

				salida = "form";
			} else {
				flash.addFlashAttribute("danger", messageSource.getMessage("text.cliente.flash.id.error", null, locale));
			}

			
		} else {
			flash.addFlashAttribute("danger", messageSource.getMessage("text.cliente.flash.id.error", null, locale));
		}

		return salida;
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/delete/{id}")
	public String eliminar(@PathVariable(value = "id") Long id,  RedirectAttributes flash, SessionStatus status, Locale locale) {

		if (id > 0) {
			Cliente cliente = clienteService.find(id);
			if ( cliente != null ) {
				if (cliente.getFoto() != null && cliente.getFoto().length() > 0) {
					if (!uploadService.delete(cliente.getFoto())) {
						logger.info("No se pudo borrar el archivo: " + cliente.getFoto());
					}
				}
				clienteService.delete(id);
				status.setComplete();
				flash.addFlashAttribute("sucess", messageSource.getMessage("text.cliente.flash.eliminar.success", null, locale));
			} else {
				flash.addFlashAttribute("danger", messageSource.getMessage("text.cliente.flash.id.error", null, locale));
			}
		} else {
			flash.addFlashAttribute("danger", messageSource.getMessage("text.cliente.flash.id.error", null, locale));
		}

		return "redirect:/listar";
	}
	
	@PreAuthorize("hasRole('ROLE_USER')")
	@GetMapping(value="/ver/{id}")
	public String ver(@PathVariable(value = "id") Long id, Model model, RedirectAttributes flash, Locale locale) {
		
		String salida = "redirect:/listar";
		Cliente cliente = clienteService.fetch(id);//clienteService.find(id);
		
		if (cliente == null) {
			flash.addFlashAttribute("danger", messageSource.getMessage("text.cliente.flash.db.error", null, locale));
			
		} else {
			model.addAttribute("titulo", "Datos del cliente " + String.valueOf(cliente.getId()) );
			model.addAttribute("tituloFactura", "Facturas del cliente " + String.valueOf(cliente.getId()));
			model.addAttribute("cliente", cliente);
			salida = "view";
		}
		
		
		return salida;
		
	}
	
	@Secured("ROLE_USER")
	@GetMapping(value="/uploads/{filename:-+}")
	public ResponseEntity<Resource> verFoto(@PathVariable String filename){
		
		
		Resource recurso = null;
		try {
			recurso = uploadService.load(filename);
		} catch (MalformedURLException | RuntimeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,"attachment; filename=\""+recurso.getFilename()+"\"" ).body(recurso);
	}
	
	private boolean hasRole(String role) {
		boolean salida=false;
		SecurityContext context = SecurityContextHolder.getContext(); 
		
		if (context != null) {
			Authentication auth = context.getAuthentication();
			if (auth != null) {
				Collection<? extends GrantedAuthority> authorities = auth.getAuthorities();
				for (GrantedAuthority authority : authorities ) {
					if (authority.getAuthority().equals(role)) {
						salida = true;
					}
				}
				
				//salida = authorities.contains(new SimpleGrantedAuthority(role)); //<--- Alternativa al for
			}
		}
		
		
		
		return salida;
	}
	

}
