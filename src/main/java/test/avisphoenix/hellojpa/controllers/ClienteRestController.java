package test.avisphoenix.hellojpa.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import test.avisphoenix.hellojpa.models.services.IClienteService;
import test.avisphoenix.hellojpa.view.xml.ClienteList;

@RestController
@RequestMapping("/api")
public class ClienteRestController {
	
	@Autowired
	private IClienteService clienteService;
	
	@GetMapping(value = "/listar")
	public ClienteList listarRest() {
		return new ClienteList(clienteService.findAll());
	}

}
