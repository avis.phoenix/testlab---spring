package test.avisphoenix.hellojpa.controllers;

import java.util.Locale;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import test.avisphoenix.hellojpa.models.entity.Cliente;
import test.avisphoenix.hellojpa.models.entity.Factura;
import test.avisphoenix.hellojpa.models.entity.ItemFactura;
import test.avisphoenix.hellojpa.models.entity.Producto;
import test.avisphoenix.hellojpa.models.services.IClienteService;
import test.avisphoenix.hellojpa.models.services.IFacturaService;
import test.avisphoenix.hellojpa.models.services.IProductoService;

@Secured("ROLE_ADMIN")
@Controller
@RequestMapping("/factura")
@SessionAttributes("factura")
public class FacturaController {
	
	@Autowired
	private IClienteService clienteService;
	
	@Autowired
	private IFacturaService facturaService;
	
	@Autowired
	private IProductoService productoService;
	
	@Autowired
	private MessageSource messageSource;
	
	@RequestMapping(value = "/form/{clienteId}")
	public String crear(@PathVariable(value = "clienteId") Long clienteId, Map<String, Object> model, RedirectAttributes flash, Locale locale) {
		String salida = "redirect:/listar";

		if ( clienteId > 0) {
			Cliente cliente = clienteService.find(clienteId);
			if (cliente != null) {
				Factura factura = new Factura();
				factura.setCliente(cliente);
				cliente.addFactura(factura);
				model.put("titulo", "Generar nueva factura");
				model.put("factura", factura);

				salida = "factura/form";
			} else {
				flash.addFlashAttribute("danger", messageSource.getMessage("text.cliente.flash.id.error", null, locale));
			}
			
		} else {
			flash.addFlashAttribute("danger", messageSource.getMessage("text.cliente.flash.id.error", null, locale));
		}

		return salida;
		
	}
	
	@RequestMapping(value = "/form", method = RequestMethod.POST)
	public String guardar(@Valid Factura factura, BindingResult result, Map<String, Object> model, RedirectAttributes flash, SessionStatus status,
			@RequestParam(name="item_id[]", required=false) Long[] itemId, @RequestParam(name="cantidad[]", required=false) Integer[] cantidad,
			Locale locale) {
		
		String salida = "factura/form";
		if (!result.hasErrors()) {
			if (itemId == null || itemId.length == 0) {
				model.put("titulo", "Generar nueva factura"); 
				model.put("danger", messageSource.getMessage("text.factura.flash.lineas.error", null, locale));
			}else {
				String mensaje = factura.getId() != null ? "editada" : "agregada";
				
				for(int i = 0; i < itemId.length; i++) {
					Producto producto = productoService.find(itemId[i]);
					ItemFactura linea = new ItemFactura();
					linea.setProducto(producto);
					linea.setCantidad(cantidad[i]);
					factura.addItemFactura(linea);
				}

				facturaService.save(factura);
				status.setComplete(); 
				flash.addFlashAttribute("sucess",  messageSource.getMessage("text.factura.flash.crear.success", null, locale));
				salida = "redirect:/ver/" + String.valueOf(factura.getCliente().getId());
			}
			
		}
		else {
			model.put("titulo", "Generar nueva factura");
		}

		return salida;
		
	}
	
	@RequestMapping(value = "/edit/{id}")
	public String editar(@PathVariable(value = "id") Long id, Map<String, Object> model, RedirectAttributes flash, Locale locale) {

		String salida = "redirect:/listar";

		if (id > 0) {
			Factura factura = facturaService.find(id);
			if (factura != null) {
				model.put("titulo", "Modificar datos del cliente");
				model.put("factura", factura);

				salida = "factura/form";
			} else { 
				flash.addFlashAttribute("danger", messageSource.getMessage("text.factura.flash.db.error", null, locale)/*"La factura con folio " + String.valueOf(id)  + " no existe."*/);
			}

			
		} else {
			flash.addFlashAttribute("danger", messageSource.getMessage("text.factura.flash.db.error", null, locale)/*"La factura con folio " + String.valueOf(id)  + " no es válido."*/);
		}

		return salida;
	}
	
	@RequestMapping(value = "/delete/{id}")
	public String eliminar(@PathVariable(value = "id") Long id,  RedirectAttributes flash, SessionStatus status, Locale locale) {

		if (id > 0) {
			Factura factura = facturaService.find(id);
			if ( factura != null ) {
				
				facturaService.delete(id);
				status.setComplete();
				flash.addFlashAttribute("sucess", messageSource.getMessage("text.factura.flash.eliminar.success", null, locale));
			} else {
				flash.addFlashAttribute("danger", messageSource.getMessage("text.factura.flash.db.error", null, locale)/*"La factura con folio " + String.valueOf(id)  + " no existe."*/);
			}
		} else {
			flash.addFlashAttribute("danger", messageSource.getMessage("text.factura.flash.db.error", null, locale)/*"La factura con folio " + String.valueOf(id)  + " no es válido."*/);
		}

		return "redirect:/listar";
	}

	
	@GetMapping(value="/ver/{id}")
	public String ver(@PathVariable(value = "id") Long id, Model model, RedirectAttributes flash, Locale locale) {
		
		String salida = "redirect:/listar";
		Factura factura = facturaService.fetch(id);//facturaService.find(id);
		
		if (factura == null) {
			flash.addFlashAttribute("danger", messageSource.getMessage("text.factura.flash.db.error", null, locale)/*"La factura con folio" + String.valueOf(id) + "no existe en la base de datos."*/);
			
		} else {
			model.addAttribute("titulo", "Detalles de la factura" );
			model.addAttribute("factura", factura);
			salida = "factura/factura";
		}
		
		
		return salida;
		
	}

}
