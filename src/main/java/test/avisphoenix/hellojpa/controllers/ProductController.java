package test.avisphoenix.hellojpa.controllers;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import test.avisphoenix.hellojpa.models.entity.Producto;
import test.avisphoenix.hellojpa.models.services.IProductoService;

@Secured("ROLE_ADMIN")
@Controller
@RequestMapping("/producto")
@SessionAttributes("producto")
public class ProductController {
	
	@Autowired
	private IProductoService productoService;
	
	@RequestMapping(value = "/edit")
	public String crear( Map<String, Object> model, RedirectAttributes flash) {
		
		Producto producto = new Producto();
		
		model.put("titulo", "Agregar producto");
		model.put("producto", producto);

		return "producto/form";
		
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public String guardar(@Valid Producto producto, BindingResult result, Map<String, Object> model, RedirectAttributes flash, SessionStatus status) {
		
		String salida = "producto/form";
		if (!result.hasErrors()) {
			String mensaje = producto.getId() != null ? "editado" : "agregado";

			productoService.save(producto);
			status.setComplete();
			flash.addFlashAttribute("sucess", "Producto " + mensaje + " correctamente.");
			salida = "redirect:/listar";
		}
		model.put("titulo", "Agregar producto");

		return salida;
		
	}
	
	@RequestMapping(value = "/edit/{id}")
	public String editar(@PathVariable(value = "id") Long id, Map<String, Object> model, RedirectAttributes flash) {

		String salida = "redirect:/listar";

		if (id > 0) {
			Producto producto = productoService.find(id);
			if (producto != null) {
				model.put("titulo", "Modificar datos del producto");
				model.put("producto", producto);

				salida = "producto/form";
			} else {
				flash.addFlashAttribute("danger", "La Producto con id " + String.valueOf(id)  + " no existe.");
			}

			
		} else {
			flash.addFlashAttribute("danger", "La Producto folio " + String.valueOf(id)  + " no es válido.");
		}

		return salida;
	}
	
	@RequestMapping(value = "/delete/{id}")
	public String eliminar(@PathVariable(value = "id") Long id,  RedirectAttributes flash, SessionStatus status) {

		if (id > 0) {
			Producto producto = productoService.find(id);
			if ( producto != null ) {
				
				productoService.delete(id);
				status.setComplete();
				flash.addFlashAttribute("sucess", "Se eliminó la Producto correctamente.");
			} else {
				flash.addFlashAttribute("danger", "La Producto con id " + String.valueOf(id)  + " no existe.");
			}
		} else {
			flash.addFlashAttribute("danger", "La Producto con id " + String.valueOf(id)  + " no es válido.");
		}

		return "redirect:/listar";
	}

	
	@GetMapping(value="/listar")
	public String ver(Model model, RedirectAttributes flash) {
		
		String salida = "redirect:/listar";
		List<Producto> productos = productoService.findAll();
		
		model.addAttribute("titulo", "Detalles de la Producto" );
		model.addAttribute("productos", productos);
		salida = "producto/listar";
		
		
		return salida;
		
	}
	
	@GetMapping(value="/cargar-productos/{term}", produces= {"application/json"})
	public @ResponseBody List<Producto> cargarProductos(@PathVariable String term ){
		return productoService.findByName(term);
	}

}
