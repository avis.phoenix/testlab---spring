package test.avisphoenix.hellojpa.view.xlsx;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellAddress;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsxView;

import test.avisphoenix.hellojpa.models.entity.Factura;
import test.avisphoenix.hellojpa.models.entity.ItemFactura;

@Component("factura/factura.xlsx")
public class FacturaXlsxView extends AbstractXlsxView{

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		MessageSourceAccessor messages = getMessageSourceAccessor();
		
		Factura factura = (Factura) model.get("factura");
		
		response.addHeader("Content-Disposition", "attachment; filename=\"" + messages.getMessage("text.factura.ver.titulo") + "_"  + factura.getCliente().getNombre() + "_" + factura.getId() + ".xlsx\"" );
		
		Sheet hoja = workbook.createSheet(messages.getMessage("text.factura.ver.titulo"));
		
		Font titleFont = workbook.createFont();
		titleFont.setBold(true);
		titleFont.setFontHeightInPoints((short) 16);
		
		Font nameFont = workbook.createFont();
		nameFont.setBold(true);
		nameFont.setFontHeightInPoints((short) 14);
		
		CellRangeAddress clienteTRange = new CellRangeAddress(0, 0, 0, 1);
		CellRangeAddress facturaTRange = new CellRangeAddress(4, 4, 0, 1);
		CellRangeAddress productoTRange = new CellRangeAddress(9, 9, 0, 3);
		
		CellStyle titleStyle = workbook.createCellStyle();
		titleStyle.setBorderBottom(BorderStyle.MEDIUM);
		titleStyle.setBorderTop(BorderStyle.MEDIUM);
		titleStyle.setBorderLeft(BorderStyle.MEDIUM);
		titleStyle.setBorderRight(BorderStyle.MEDIUM);
		titleStyle.setFont(titleFont);
		titleStyle.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.index);
		titleStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		
		CellStyle nameStyle = workbook.createCellStyle();
		nameStyle.setBorderBottom(BorderStyle.MEDIUM);
		nameStyle.setBorderTop(BorderStyle.MEDIUM);
		nameStyle.setBorderLeft(BorderStyle.MEDIUM);
		nameStyle.setBorderRight(BorderStyle.MEDIUM);
		nameStyle.setFont(nameFont);
		nameStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index);
		nameStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		
		CellStyle bodyStyle = workbook.createCellStyle();
		bodyStyle.setBorderBottom(BorderStyle.MEDIUM);
		bodyStyle.setBorderTop(BorderStyle.MEDIUM);
		bodyStyle.setBorderLeft(BorderStyle.MEDIUM);
		bodyStyle.setBorderRight(BorderStyle.MEDIUM);

		hoja.addMergedRegion(clienteTRange);
		hoja.addMergedRegion(facturaTRange);
		hoja.addMergedRegion(productoTRange);
		
		Row fila = hoja.createRow(0);
		Cell celda = fila.createCell(0);
		celda.setCellValue(messages.getMessage("text.factura.ver.datos.cliente"));
		celda.setCellStyle(titleStyle);
		
		fila = hoja.createRow(1);
		celda = fila.createCell(0);
		celda.setCellValue(messages.getMessage("text.cliente.nombre"));
		celda.setCellStyle(nameStyle);
		
		celda = fila.createCell(1);
		celda.setCellValue(factura.getCliente().getNombre() + ' ' + factura.getCliente().getApellido());
		celda.setCellStyle(bodyStyle);
		
		fila = hoja.createRow(2);
		celda = fila.createCell(0);
		celda.setCellValue(messages.getMessage("text.cliente.email"));
		celda.setCellStyle(nameStyle);
		
		celda = fila.createCell(1);
		celda.setCellValue(factura.getCliente().getEmail());
		celda.setCellStyle(bodyStyle);
		
		fila = hoja.createRow(4);
		celda = fila.createCell(0);
		celda.setCellValue(messages.getMessage("text.factura.ver.datos.factura"));
		celda.setCellStyle(titleStyle);
		
		fila = hoja.createRow(5);
		celda = fila.createCell(0);
		celda.setCellValue(messages.getMessage("text.cliente.factura.folio"));
		celda.setCellStyle(nameStyle);
		celda = fila.createCell(1);
		celda.setCellValue(factura.getId());
		celda.setCellStyle(bodyStyle);
		
		fila = hoja.createRow(6);
		celda = fila.createCell(0);
		celda.setCellValue(messages.getMessage("text.cliente.factura.descripcion"));
		celda.setCellStyle(nameStyle);
		celda = fila.createCell(1);
		celda.setCellValue(factura.getDescripcion());
		celda.setCellStyle(bodyStyle);
		
		fila = hoja.createRow(7);
		celda = fila.createCell(0);
		celda.setCellValue(messages.getMessage("text.cliente.factura.fecha"));
		celda.setCellStyle(nameStyle);
		celda = fila.createCell(1);
		celda.setCellValue(factura.getCreateAt());
		celda.setCellStyle(bodyStyle);
		
		fila = hoja.createRow(9);
		celda = fila.createCell(0);
		celda.setCellValue(messages.getMessage("text.factura.ver.datos.factura"));
		celda.setCellStyle(titleStyle);
		
		
		fila = hoja.createRow(10);
		celda = fila.createCell(0);
		celda.setCellValue(messages.getMessage("text.factura.form.item.nombre"));
		celda.setCellStyle(nameStyle);
		celda = fila.createCell(1);
		celda.setCellValue(messages.getMessage("text.factura.form.item.precio"));
		celda.setCellStyle(nameStyle);
		celda = fila.createCell(2);
		celda.setCellValue(messages.getMessage("text.factura.form.item.cantidad"));
		celda.setCellStyle(nameStyle);
		celda = fila.createCell(3);
		celda.setCellValue(messages.getMessage("text.factura.form.item.total"));
		celda.setCellStyle(nameStyle);
		
		int i = 11;
		for (ItemFactura item: factura.getLineas()) {
			fila = hoja.createRow(i);
			celda = fila.createCell(0);
			celda.setCellValue(item.getProducto().getNombre());
			celda.setCellStyle(bodyStyle);
			celda = fila.createCell(1);
			celda.setCellValue(item.getProducto().getPrecio());
			celda.setCellStyle(bodyStyle);
			celda = fila.createCell(2);
			celda.setCellValue(item.getCantidad());
			celda.setCellStyle(bodyStyle);
			celda = fila.createCell(3);
			celda.setCellValue(item.getImporte());
			celda.setCellStyle(bodyStyle);
			i = i + 1;
		}
		fila = hoja.createRow(i);
		celda = fila.createCell(2);
		celda.setCellValue(messages.getMessage("text.factura.form.total"));
		celda.setCellStyle(nameStyle);
		celda = fila.createCell(3);
		celda.setCellValue(factura.getTotal());
		celda.setCellStyle(bodyStyle);
		
		RegionUtil.setBorderBottom(BorderStyle.MEDIUM, clienteTRange, hoja);
		RegionUtil.setBorderTop(BorderStyle.MEDIUM, clienteTRange, hoja);
		RegionUtil.setBorderLeft(BorderStyle.MEDIUM, clienteTRange, hoja);
		RegionUtil.setBorderRight(BorderStyle.MEDIUM, clienteTRange, hoja);
		
		RegionUtil.setBorderBottom(BorderStyle.MEDIUM, facturaTRange, hoja);
		RegionUtil.setBorderTop(BorderStyle.MEDIUM, facturaTRange, hoja);
		RegionUtil.setBorderLeft(BorderStyle.MEDIUM, facturaTRange, hoja);
		RegionUtil.setBorderRight(BorderStyle.MEDIUM, facturaTRange, hoja);
		
		RegionUtil.setBorderBottom(BorderStyle.MEDIUM, productoTRange, hoja);
		RegionUtil.setBorderTop(BorderStyle.MEDIUM, productoTRange, hoja);
		RegionUtil.setBorderLeft(BorderStyle.MEDIUM, productoTRange, hoja);
		RegionUtil.setBorderRight(BorderStyle.MEDIUM, productoTRange, hoja);
		
		hoja.autoSizeColumn(0);
		hoja.autoSizeColumn(1);
		hoja.autoSizeColumn(2);
		
	}

}
