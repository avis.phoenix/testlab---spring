package test.avisphoenix.hellojpa.view.xml;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.xml.MarshallingView;

import test.avisphoenix.hellojpa.models.entity.Cliente;

@Component("listar.xml")
public class ClienteXmlView extends MarshallingView{
	
	@Autowired
	public ClienteXmlView(Jaxb2Marshaller marshaller) {
		super(marshaller);
		// TODO Auto-generated constructor stub
	}


	@SuppressWarnings("unchecked")
	@Override
	protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		Page<Cliente> clientes = (Page<Cliente>) model.get("clientes");
		
		model.remove("titulo");
		model.remove("clientes");
		model.remove("page");
		
		model.put("clienteList", new ClienteList(clientes.getContent()));
		super.renderMergedOutputModel(model, request, response);
	}

	
	
	

}
