package test.avisphoenix.hellojpa.view.pdf;

import java.awt.Color;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import com.lowagie.text.Document;
import com.lowagie.text.Font;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import test.avisphoenix.hellojpa.models.entity.Factura;
import test.avisphoenix.hellojpa.models.entity.ItemFactura;

@Component("factura/factura")
public class FacturaPdfView extends AbstractPdfView {
	
	/*@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private LocaleResolver localeResolver;*/

	@Override
	protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		/*Locale locale = localeResolver.resolveLocale(request);*/
		
		MessageSourceAccessor messages = getMessageSourceAccessor();
		
		Factura factura = (Factura) model.get("factura");
		
		PdfPTable table = new PdfPTable(2);
		
		Color CTitle = new Color(176,190,197);
		Color CName = new Color(207,216,220);
		Font FTitle = new Font(Font.TIMES_ROMAN, 16, Font.BOLD);
		Font FName = new Font(Font.TIMES_ROMAN, 14, Font.BOLD);
		
		PdfPCell tempCell;
		
		PdfPCell cell = new PdfPCell(new Phrase(messages.getMessage("text.factura.ver.datos.cliente"), FTitle));
		cell.setBackgroundColor(CTitle);
		cell.setPadding(7);
        cell.setColspan(2);
        table.addCell(cell);
        
        tempCell = new PdfPCell(new Phrase(messages.getMessage("text.cliente.nombre"), FName));
        tempCell.setBackgroundColor(CName);
        table.addCell(tempCell);
        
        table.addCell(factura.getCliente().getNombre() + ' ' + factura.getCliente().getApellido());
        
        tempCell = new PdfPCell(new Phrase(messages.getMessage("text.cliente.email"), FName));
        tempCell.setBackgroundColor(CName);
        table.addCell(tempCell);
        
        table.addCell(factura.getCliente().getEmail());
        table.setSpacingAfter(20);
        
        PdfPTable table2 = new PdfPTable(2);
		
		PdfPCell cellT2 = new PdfPCell(new Phrase(messages.getMessage("text.factura.ver.datos.factura"), FTitle));
		cellT2.setBackgroundColor(CTitle);
		cellT2.setPadding(7);
		cellT2.setColspan(2);
		table2.addCell(cellT2);
		
		tempCell = new PdfPCell(new Phrase(messages.getMessage("text.cliente.factura.folio"), FName));
        tempCell.setBackgroundColor(CName);
        table2.addCell(tempCell);
        
		table2.addCell(String.valueOf(factura.getId()));
		
		tempCell = new PdfPCell(new Phrase(messages.getMessage("text.cliente.factura.descripcion"), FName));
        tempCell.setBackgroundColor(CName);
        table2.addCell(tempCell);

		table2.addCell(factura.getDescripcion());
		
		tempCell = new PdfPCell(new Phrase(messages.getMessage("text.cliente.factura.fecha"), FName));
        tempCell.setBackgroundColor(CName);
        table.addCell(tempCell);

		table2.addCell(factura.getCreateAt().toString());
		table2.setSpacingAfter(20);
		
		PdfPTable table3 = new PdfPTable(4);
		
		table3.setWidths(new float[] {3.5f, 1f, 0.95f, 1f});
		
		PdfPCell cellT3 = new PdfPCell(new Phrase(messages.getMessage("text.factura.ver.datos.factura"), FTitle));
		cellT3.setBackgroundColor(CTitle);
		cellT3.setPadding(7);
		cellT3.setColspan(4);
		table3.addCell(cellT3);
		
		tempCell = new PdfPCell(new Phrase(messages.getMessage("text.factura.form.item.nombre"), FName));
        tempCell.setBackgroundColor(CName);
        table3.addCell(tempCell);
        tempCell = new PdfPCell(new Phrase(messages.getMessage("text.factura.form.item.precio"), FName));
        tempCell.setBackgroundColor(CName);
        table3.addCell(tempCell);
        tempCell = new PdfPCell(new Phrase(messages.getMessage("text.factura.form.item.cantidad"), FName));
        tempCell.setBackgroundColor(CName);
        table3.addCell(tempCell);
        tempCell = new PdfPCell(new Phrase(messages.getMessage("text.factura.form.item.total"), FName));
        tempCell.setBackgroundColor(CName);
        table3.addCell(tempCell);

		for (ItemFactura item: factura.getLineas()) {
			table3.addCell(item.getProducto().getNombre());
			table3.addCell(String.valueOf(item.getProducto().getPrecio()));
			table3.addCell(String.valueOf(item.getCantidad()));
			table3.addCell(String.valueOf(item.getImporte()));
		}
		PdfPCell cellGT = new PdfPCell(new Phrase(messages.getMessage("text.factura.form.total"), FName));
		cellGT.setBackgroundColor(CTitle);
		cellGT.setColspan(3);
		cellGT.setPadding(5);
		cellGT.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
		table3.addCell(cellGT);
		table3.addCell(new Phrase(factura.getTotal().toString(), FName));
		table3.setSpacingAfter(20);
		
		document.add(table);
		document.add(table2);
		document.add(table3);
        
	}


}
