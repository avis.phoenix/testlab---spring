package test.avisphoenix.hellojpa;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
/*import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.User.UserBuilder;*/
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
/*import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;*/

import test.avisphoenix.hellojpa.auth.handler.LoginSuccessHandler;
import test.avisphoenix.hellojpa.models.services.JpaUserDetailsServices;

@EnableGlobalMethodSecurity(securedEnabled=true, prePostEnabled=true)
@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private LoginSuccessHandler loginSuccessHandler;
	
	@Autowired
	private JpaUserDetailsServices userDetailsService;
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	/*@Autowired
	private DataSource dataSource;*/
	
	@Autowired
	public void configurerGlobal(AuthenticationManagerBuilder build) throws Exception {
		
		/*PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
		UserBuilder users = User.builder().passwordEncoder(encoder::encode);
		
		
		build.inMemoryAuthentication()
		.withUser(users.username("admin").password("123456").roles("ADMIN","USER"))
		.withUser(users.username("andres").password("123456").roles("USER"));*/
		
		/*build.jdbcAuthentication().dataSource(dataSource).passwordEncoder(passwordEncoder)
		.usersByUsernameQuery("select username, password, enabled from db_springboot_tester.users where username = ?")
		.authoritiesByUsernameQuery("select u.username, a.authority from db_springboot_tester.authorities a inner join db_springboot_tester.users u on (a.user_id = u.id) where u.username = ?");*/
		
		build.userDetailsService(userDetailsService)
		.passwordEncoder(passwordEncoder);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/","/css/**","/js/**","/images/**", "/listar", "/locale", "/api/**").permitAll()
		/*.antMatchers("/ver/**", "/uploads/**").hasAnyRole("USER")
		.antMatchers("/form/**","/delete/**","/producto/**", "/factura/**").hasAnyRole("ADMIN")*/
		.anyRequest().authenticated()
		.and()
		.formLogin().successHandler(loginSuccessHandler).loginPage("/login").permitAll()
		.and().logout().permitAll()
		.and()
		.exceptionHandling().accessDeniedPage("/error_403");
	}
	

}
