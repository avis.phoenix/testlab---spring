﻿package test.avisphoenix.hellojpa.models.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import test.avisphoenix.hellojpa.models.entity.Cliente;

public interface IClienteService {

	public List<Cliente> findAll();
	
	public Page<Cliente> findAll(Pageable pageable);

	public void save(Cliente cliente);

	public Cliente find(Long id);

	public void delete(Long id);
	
	public Cliente fetch(Long id);

}
