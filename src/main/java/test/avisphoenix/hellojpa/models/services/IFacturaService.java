package test.avisphoenix.hellojpa.models.services;

import java.util.List;

import test.avisphoenix.hellojpa.models.entity.Factura;

public interface IFacturaService {
	
	public List<Factura> findAll();

	public void save(Factura factura);

	public Factura find(Long id);

	public void delete(Long id);
	
	public Factura fetch(Long id);

}
