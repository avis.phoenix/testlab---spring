package test.avisphoenix.hellojpa.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import test.avisphoenix.hellojpa.models.dao.IProductoDao;
import test.avisphoenix.hellojpa.models.entity.Producto;

@Service
public class ProductoServiceImpl implements IProductoService {

	// Al ser un crud no es necesario tener implementación
	@Autowired
	private IProductoDao productoDao;

	@Override
	public List<Producto> findAll() {
		
		return (List<Producto>)productoDao.findAll();
	}

	@Override
	public void save(Producto producto) {
		productoDao.save(producto);
	}

	@Override
	public Producto find(Long id) {
		return productoDao.findById(id).orElse(null);
	}

	@Override
	public void delete(Long id) {
		productoDao.deleteById(id);
	}

	@Override
	public List<Producto> findByName(String term) {
		// TODO Auto-generated method stub
		return productoDao.findByNombreLikeIgnoreCase('%' + term + '%');
	}

}
