package test.avisphoenix.hellojpa.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import test.avisphoenix.hellojpa.models.dao.IFacturaDao;
import test.avisphoenix.hellojpa.models.entity.Factura;

@Service
public class FacturaServiceImpl implements IFacturaService {

	// Al ser un crud no es necesario tener implementación
	@Autowired
	private IFacturaDao facturaDao;

	@Override
	public List<Factura> findAll() {
		return (List<Factura>) facturaDao.findAll();
	}

	@Override
	@Transactional
	public void save(Factura factura) {
		facturaDao.save(factura);

	}

	@Override
	@Transactional(readOnly = true)
	public Factura find(Long id) {

		return facturaDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		facturaDao.deleteById(id);

	}

	@Override
	@Transactional(readOnly = true)
	public Factura fetch(Long id) {
		// TODO Auto-generated method stub
		return facturaDao.fetchByIdWithClienteWithItemFacturaWithProducto(id);
	}

}
