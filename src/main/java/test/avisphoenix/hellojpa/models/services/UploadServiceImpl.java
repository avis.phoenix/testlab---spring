﻿package test.avisphoenix.hellojpa.models.services;

import java.io.File;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
public class UploadServiceImpl implements IUploadFileService {
	
	public static final String UPLOAD_FOLDER = "uploads";
	
	private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());

	@Override
	public Resource load(String filename) throws RuntimeException, MalformedURLException{
		Path pathFoto = getPath(filename);
		
		Resource recurso  = null;
		
		recurso = new UrlResource(pathFoto.toUri());
		if (!recurso.exists() || !recurso.isReadable()  ) {
			throw new RuntimeException("Error al cargar la imagen");	
		}
		
		return recurso;
	}

	@Override
	public String copy(MultipartFile file) throws IOException {
		String uniqueFileName = UUID.randomUUID().toString() + "_" + file.getOriginalFilename();
		//Path rutaCompleta = Paths.get(rootPath + "/" + uniqueFileName;
		Path rutaCompleta = getPath(uniqueFileName);
		/*byte[] bytes = foto.getBytes();
		Files.write(rutaCompleta,  bytes);*/
		Files.copy(file.getInputStream(), rutaCompleta);
		return uniqueFileName;
	}

	@Override
	public boolean delete(String filename) {
		boolean salida = false;
		Path filePath = getPath(filename);
		
		logger.info("Intentamos borrar el archivo: " + filePath.toAbsolutePath().toString());
		
		File archivo = filePath.toFile();
		
		if (archivo.exists() && archivo.canRead()) {
			salida = archivo.delete();
		}
		return salida;
	}

	@Override
	public Path getPath(String filename) {
		// TODO Auto-generated method stub
		return Paths.get(UPLOAD_FOLDER).resolve(filename).toAbsolutePath();
	}

	@Override
	public void deleteAll() {
		FileSystemUtils.deleteRecursively(Paths.get(UPLOAD_FOLDER).toFile());
		
	}

	@Override
	public void init() throws IOException {
		// TODO Auto-generated method stub
		Files.createDirectory(Paths.get(UPLOAD_FOLDER));
	}

}
