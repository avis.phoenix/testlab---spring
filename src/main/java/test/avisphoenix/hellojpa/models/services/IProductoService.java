package test.avisphoenix.hellojpa.models.services;

import java.util.List;

import test.avisphoenix.hellojpa.models.entity.Producto;

public interface IProductoService {
	
	public List<Producto> findAll();

	public void save(Producto factura);

	public Producto find(Long id);

	public void delete(Long id);
	
	public List<Producto> findByName(String term);
}
