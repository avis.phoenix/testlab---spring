package test.avisphoenix.hellojpa.models.dao;

import org.springframework.data.repository.CrudRepository;

import test.avisphoenix.hellojpa.models.entity.Usuario;

public interface IUsuarioDao extends CrudRepository<Usuario, Long>{

	public Usuario findByUsername(String username);
}
