﻿package test.avisphoenix.hellojpa.models.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlTransient;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="facturas")
public class Factura  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@NotEmpty
	private String descripcion;
	
	private String observaciones;
	
	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name="create_at")
	@DateTimeFormat(pattern="yyyy/MM/dd")
	private Date createAt;
	
	@NotNull
	@ManyToOne(fetch=FetchType.LAZY)
	@JsonBackReference
	private Cliente cliente;
	
	@NotNull
	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinColumn(name="factura_id")
	private List<ItemFactura> lineas;
	
	@PrePersist
	public void prePersist() {
		createAt = new Date();
	}
	
	public Factura() {
		lineas = new ArrayList<ItemFactura>();
	}
	
	public Float getTotal() {
		Float total = 0.0f;
		
		for(int i =0; i < lineas.size(); i++) {
			total += lineas.get(i).getImporte();
		}
		return total;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	@XmlTransient
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<ItemFactura> getLineas() {
		return lineas;
	}

	public void setLineas(List<ItemFactura> lineas) {
		this.lineas = lineas;
	}
	
	public void addItemFactura(ItemFactura linea) {
		lineas.add(linea);
	}

}
